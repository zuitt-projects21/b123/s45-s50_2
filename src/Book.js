import Author from './Author';
import Title from './Title';
import Image from './Image';


const Book = () => {
  return (
    <section className="book">
      <Author/>
      <Title/>
      <Image/>
    </section>
  );
};

export default Book;