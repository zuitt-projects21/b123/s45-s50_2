const Author = () => {
  const authorStyles = {
    color: '#617d98',
    fontSize: '0.75rem',
    marginTop: '0.25rem'
  };
  const authorName = 'Antoine de Saint-Exupery'; 
  //note:css style keywords wont work in react.(ex. font-size)
  return <h5 style={authorStyles}>{authorName}</h5>
};

export default Author;