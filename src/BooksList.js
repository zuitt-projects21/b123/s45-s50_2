import Book from './Book';
import './index.css';

const BooksList = () => {

  return (
    <main className="book-list">
      <Book/>
      <Book/>
      <Book/>
      <Book/>
      <Book/>
      <Book/>
      <Book/>
      <Book/>
      <Book/>
    </main>
  );

};

export default BooksList;