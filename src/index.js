import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import BooksList from './BooksList';


ReactDOM.render(<BooksList/>, document.getElementById('root'));

//how to render all 3 at the same time: (make a funct)
//components are functions(should return something)
//<react.Fragments><
