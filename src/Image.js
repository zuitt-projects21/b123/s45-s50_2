const Image = () => {
  const imageSource = 'https://i.gr-assets.com/images/S/compressed.photo.goodreads.com/books/1367545443l/157993.jpg';
  const imageAlternate = 'A cover page of a book.';

  return <img src={imageSource} alt={imageAlternate} />;
};

export default Image;